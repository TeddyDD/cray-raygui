# Style Load/Save
# This functions are not generated from header automatically

lib LibRaygui
  # Save style file (.rgs), text or binary
  fun save_style = GuiSaveStyle(filename : LibC::Char*, binary : Bool) : Void

  # Load style file (.rgs), text or binary
  fun load_style = GuiLoadStyle(filename : LibC::Char*) : Void

  # Load style from a color palette array (14 values required)
  fun load_style_pallete = GuiLoadStylePalette(pallete : LibC::Int) : Void

  # Load style from an image palette file (64x16)
  fun load_style_pallete_image = GuiLoadStylePaletteImage(filename : LibC::Char*) : Void

  # Updates full style properties set with generic values
  fun update_style_complete = GuiUpdateStyleComplete : Void
end
