# DO NOT INCLUDE THIS FILE DIRECTLY!

lib LibRaygui

  fun color_bar_alpha = GuiColorBarAlpha(bounds : LibRay::Rectangle, alpha : LibC::Float) : LibC::Float
  fun color_bar_hue = GuiColorBarHue(bounds : LibRay::Rectangle, hue : LibC::Float) : LibC::Float
  fun color_panel = GuiColorPanel(bounds : LibRay::Rectangle, color : LibRay::Color) : LibRay::Color
  fun grid = GuiGrid(bounds : LibRay::Rectangle, spacing : LibC::Int, subdivs : LibC::Int, snap : Bool) : LibRay::Vector2

  fun button = GuiButton(bounds : LibRay::Rectangle, text : LibC::Char*) : Bool
  fun check_box = GuiCheckBox(bounds : LibRay::Rectangle, checked : Bool) : Bool
  fun check_box_ex = GuiCheckBoxEx(bounds : LibRay::Rectangle, checked : Bool, text : LibC::Char*) : Bool
  fun color_picker = GuiColorPicker(bounds : LibRay::Rectangle, color : LibRay::Color) : LibRay::Color
  fun combo_box = GuiComboBox(bounds : LibRay::Rectangle, text : LibC::Char**, count : LibC::Int, active : LibC::Int) : LibC::Int
  fun disable = GuiDisable
  fun dropdown_box = GuiDropdownBox(bounds : LibRay::Rectangle, text : LibC::Char**, count : LibC::Int, active : LibC::Int) : LibC::Int
  fun dummy_rec = GuiDummyRec(bounds : LibRay::Rectangle, text : LibC::Char*)
  fun enable = GuiEnable
  fun fade = GuiFade(alpha : LibC::Float)
  fun get_style_property = GuiGetStyleProperty(property : LibC::Int) : LibC::Int
  fun group_box = GuiGroupBox(bounds : LibRay::Rectangle, text : LibC::Char*)
  fun image_button = GuiImageButton(bounds : LibRay::Rectangle, texture : LibRay::Texture2D) : Bool
  fun image_button_ex = GuiImageButtonEx(bounds : LibRay::Rectangle, texture : LibRay::Texture2D, tex_source : LibRay::Rectangle, text : LibC::Char*) : Bool
  fun label = GuiLabel(bounds : LibRay::Rectangle, text : LibC::Char*)
  fun label_button = GuiLabelButton(bounds : LibRay::Rectangle, text : LibC::Char*) : Bool
  fun line = GuiLine(bounds : LibRay::Rectangle, thick : LibC::Int)
  fun list_view = GuiListView(bounds : LibRay::Rectangle, text : LibC::Char**, count : LibC::Int, active : LibC::Int) : LibC::Int
  fun message_box = GuiMessageBox(bounds : LibRay::Rectangle, window_title : LibC::Char*, message : LibC::Char*) : Bool
  fun panel = GuiPanel(bounds : LibRay::Rectangle)
  fun progress_bar = GuiProgressBar(bounds : LibRay::Rectangle, value : LibC::Float, min_value : LibC::Float, max_value : LibC::Float) : LibC::Float
  fun progress_bar_ex = GuiProgressBarEx(bounds : LibRay::Rectangle, value : LibC::Float, min_value : LibC::Float, max_value : LibC::Float, show_value : Bool) : LibC::Float
  fun scroll_panel = GuiScrollPanel(bounds : LibRay::Rectangle, content : LibRay::Rectangle, view_scroll : LibRay::Vector2) : LibRay::Vector2
  fun set_style_property = GuiSetStyleProperty(property : LibC::Int, value : LibC::Int)
  fun slider = GuiSlider(bounds : LibRay::Rectangle, value : LibC::Float, min_value : LibC::Float, max_value : LibC::Float) : LibC::Float
  fun slider_bar = GuiSliderBar(bounds : LibRay::Rectangle, value : LibC::Float, min_value : LibC::Float, max_value : LibC::Float) : LibC::Float
  fun slider_bar_ex = GuiSliderBarEx(bounds : LibRay::Rectangle, value : LibC::Float, min_value : LibC::Float, max_value : LibC::Float, text : LibC::Char*, show_value : Bool) : LibC::Float
  fun slider_ex = GuiSliderEx(bounds : LibRay::Rectangle, value : LibC::Float, min_value : LibC::Float, max_value : LibC::Float, text : LibC::Char*, show_value : Bool) : LibC::Float
  fun spinner = GuiSpinner(bounds : LibRay::Rectangle, value : LibC::Int, max_value : LibC::Int, btn_width : LibC::Int) : LibC::Int
  fun status_bar = GuiStatusBar(bounds : LibRay::Rectangle, text : LibC::Char*, offset_x : LibC::Int)
  fun text_box = GuiTextBox(bounds : LibRay::Rectangle, text : LibC::Char*, text_size : LibC::Int, free_edit : Bool) : Bool
  fun text_box_multi = GuiTextBoxMulti(bounds : LibRay::Rectangle, text : LibC::Char*, text_size : LibC::Int, edit_mode : Bool) : Bool
  fun toggle_button = GuiToggleButton(bounds : LibRay::Rectangle, text : LibC::Char*, toggle : Bool) : Bool
  fun toggle_group = GuiToggleGroup(bounds : LibRay::Rectangle, text : LibC::Char**, count : LibC::Int, active : LibC::Int) : LibC::Int
  fun value_box = GuiValueBox(bounds : LibRay::Rectangle, value : LibC::Int, max_value : LibC::Int) : LibC::Int
  fun window_box = GuiWindowBox(bounds : LibRay::Rectangle, text : LibC::Char*) : Bool

end
