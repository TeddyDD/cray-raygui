# cray-raygui

![icon](cray-raygui.png)

[Raygui][raygui] bindings for Crystal, meant to be used with `cray` library. Cray is [Raylib][raylib] bindings.

## Installation

Add this to your application's `shard.yml`:

```yaml
dependencies:
  cray-raygui:
    gitlab: TeddyDD/cray-raygui
    branch: raylib-2
```

## Usage

```crystal
require "cray"
require "cray_raygui"
```

TODO: Write usage instructions here

## Development

Bindings are generated using [libgen](https://github.com/olbat/libgen) tool.
Generating bindings from scratch is manual process for simplicity. Fortunetly, for normal usage it is not required, and small chagnes in Raygui could be covered manually on bindigs side.

 It looks like this:

1. Install and build libgen: https://github.com/olbat/libgen
2. Download [Raygui][raygui] and copy `src/raygui.h` to `src/cray_raygui/raygui.h`
3. Modify `raygui.h`: replace `#define RAYGUI_STANDALONE` with
```c
#define RAYGUI_IMPLEMENTATION
#define RAYGUI_STYLE_SAVE_LOAD
```
4. Generate bindings using libgen: `libgen ./lib-raygui.yml`
5. libgen will create two files in `src/cray_raygui`
  - generated/cray_raygui/application.cr
  - generated/cray_raygui/cray_raygui.cr
6. Now you have to combine functions from those files into single `src/cray_raygui/gui.cr` file. Beware of duplicates. Don't copy type definitions, they are declared in `LibRay` in cray.
7. Fix type names (Rectangle -> LibRay::Rectangle, same for Vector2, Color)
8. Remove generated files
9. Remove `gui_` prefixes from `gui.cr`, who needs that?
10. Bind load/save functions manually (`src/cray_raygui/load_save.cr`)
11. Create enums definitions in `src/cray_raylib/enums.cr`

As you can see process of generating bindings from scratch is quite complicated. It's not needed for using library, and in the future I might write script to automate it.

## Contributing
1. Fork it (<https://gitlab.com/TeddyDD/cray-raygui/fork>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Format crystal files with `crystal tool format **.cr`
4. Commit your changes (`git commit -am 'Add some feature'`)
5. Push to the branch (`git push origin my-new-feature`)
6. Create a new Pull Request

## Contributors

- [TeddyDD](https://gitlab.com/TeddyDD) Daniel Lewan - creator, maintainer

[raylib]: https://github.com/raysan5/raylib
[raygui]: https://github.com/raysan5/raygui 