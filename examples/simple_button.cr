require "cray"
require "../src/cray_raygui"

LibRay.set_target_fps 60
LibRay.set_config_flags LibRay::FLAG_WINDOW_RESIZABLE
LibRay.init_window 640, 480, "Example: Simple button"

rect = LibRay::Rectangle.new(
  x: 10,
  y: 10,
  width: 100,
  height: 100
)

while !LibRay.window_should_close?
  LibRay.begin_drawing
  LibRay.clear_background LibRay::WHITE

  LibRaygui.button(rect, "Hello World!")

  LibRay.end_drawing
end

LibRay.close_window
